# fixme: convert to automake

DOCBOOK_ARTICLE_XSL = /usr/share/xml/docbook/stylesheet/nwalsh/xhtml/docbook.xsl
XSLTPROC = xsltproc

GUIDE = hackers.html
GUIDE_IMAGES = figures/valac-link.png figures/valac-data.png


all: $(GUIDE)

$(GUIDE): $(GUIDE_IMAGES)

figures/%.png: %.svg
	inkscape --without-gui --export-area-drawing --export-png=$@ $^

%.html: %.xml
	$(XSLTPROC) -o $@ $(DOCBOOK_ARTICLE_XSL) $<

clean:
	rm -f $(GUIDE)
